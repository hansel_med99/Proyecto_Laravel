<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function list() {
        $user = User::find(1);
        $list = Product::all();
        return view('usuarios', [
            'productos' => $list
        ]);
    }
}
