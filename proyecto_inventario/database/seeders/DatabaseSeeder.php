<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Product;
use App\Models\User;
use Database\Factories\ProductFactory;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $user = User::factory(20)
        ->has(Product::factory()->count(3))
        ->create();

         \App\Models\User::factory() 
         ->has(Product::factory()->count(3))
        ->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
         ]);

    
       
    }
}
